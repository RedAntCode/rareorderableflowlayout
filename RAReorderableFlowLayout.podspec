Pod::Spec.new do |s|
  s.name             = "RAReorderableFlowLayout"
  s.version          = "0.2.0"
  s.summary          = "UICollectionViewFlowLayout extension to support reordering of cells."
  s.description      = <<-DESC
                      This is a fork of [`LXReorderableCollectionViewFlowLayout`](https://github.com/lxcid/LXReorderableCollectionViewFlowLayout). Extends `UICollectionViewFlowLayout` to support reordering of cells.
                       DESC
  s.homepage         = "https://bitbucket.org/RedAntCode/RAReorderableFlowLayout"
  s.license          = 'MIT'
  s.author           = { "Ikhsan Assaat" => "ikhsan.assaat@redant.com" }
  s.source           = { :git => "https://bitbucket.org/RedAntCode/RAReorderableFlowLayout.git", :tag => s.version.to_s }

  s.platform     = :ios, '7.0'
  s.requires_arc = true

  s.source_files = 'RAReorderableFlowLayout/*.{h,m}'
  s.public_header_files = 'RAReorderableFlowLayout/*.h'
  s.frameworks = 'UIKit', 'CoreGraphics'
end
