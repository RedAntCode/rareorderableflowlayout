# RAReorderableFlowLayout

This is a fork of [`LXReorderableCollectionViewFlowLayout`](https://github.com/lxcid/LXReorderableCollectionViewFlowLayout). Extends `UICollectionViewFlowLayout` to support reordering of cells.

Why implement reordering in the flow layout? To make the integration a breeze! Setup is really simple, set this class as the flow layout of your `UICollectionView` in your nib/storyboard/code and your done. No need to do anything on your collection view cell, or view controllers. This class also provides useful delegate methods that you can implement.

![preview](Screenshot/RAReorderableFlowLayout.gif)

## Features

 - Long press on cell to invoke reordering capability.
 - When reordering capability is invoked, fade the selected cell from highlighted to normal state.
 - Drag around the selected cell to move it to the desired location, the other cells adjust accordingly. Callback in the form of delegate methods are invoked.
 - Drag selected cell to the edges, depending on scroll direction, scroll in the desired direction.
 - Release to stop reordering.
 - Cancel reordering through delegate
 - Highlighting the dragged cell


## Installation

Put this into your `Podfile`

```
source 'https://bitbucket.org/RedAntCode/specs'

pod 'RAReorderableFlowLayout'
```

## Usage

- Set your `UICollectionView`'s flow layout to `RAReorderableFlowLayout` in your nib/storyboard/code.
- Implement delegate as needed
- For further usage, review the sample code
