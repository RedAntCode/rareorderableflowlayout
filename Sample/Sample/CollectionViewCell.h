#import <UIKit/UIKit.h>

@interface CollectionViewCell : UICollectionViewCell

- (void)setText:(NSString *)text;

@end
