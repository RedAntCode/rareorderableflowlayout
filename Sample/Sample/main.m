//
//  main.m
//  Sample
//
//  Created by M Ikhsan Assaat on 11/05/2015.
//  Copyright (c) 2015 Ikhsan Assaat. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
