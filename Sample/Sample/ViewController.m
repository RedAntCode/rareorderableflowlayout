#import "ViewController.h"
#import "CollectionViewCell.h"
#import "RAReorderableFlowLayout.h"

static NSString *const kCellIdentifier = @"Cell";

@interface ViewController () <UICollectionViewDataSource, UICollectionViewDelegate, RAReorderableViewDelegate>

@property (nonatomic, strong) UICollectionView *collectionView;
@property (nonatomic, strong) RAReorderableFlowLayout *flowLayout;
@property (nonatomic, weak) UIButton *binButton;

@property (nonatomic, strong) NSMutableArray *items;
@property (nonatomic, assign) BOOL shouldDeleteItem;

@end

@implementation ViewController

#pragma mark - Lazy Instantiation

- (UICollectionView *)collectionView {
    if (!_collectionView) {
        _collectionView = [[UICollectionView alloc] initWithFrame:self.view.bounds collectionViewLayout:self.flowLayout];
        _collectionView.delegate = self;
        _collectionView.dataSource = self;
        _collectionView.indicatorStyle = UIScrollViewIndicatorStyleWhite;
    }

    return _collectionView;
}

- (RAReorderableFlowLayout *)flowLayout {
    if (!_flowLayout) {
        _flowLayout = [[RAReorderableFlowLayout alloc] init];

        _flowLayout.itemSize = CGSizeMake(120.0f, 120.0f);
        _flowLayout.sectionInset = UIEdgeInsetsMake(40.0f, 30.0f, 30.0f, 30.0f);
        _flowLayout.minimumInteritemSpacing = 10.0f;
        _flowLayout.minimumLineSpacing = 30.0f;
    }
    return _flowLayout;
}

#pragma mark - View Lifecycle

- (void)viewDidLoad {
    [super viewDidLoad];

    self.title = @"Reorderable Flow Layout";
    self.view.backgroundColor = [UIColor darkGrayColor];
    self.items = [self generateItems];

    [self setupCollectionView];
    [self setupBin];
    [self setupAddButton];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark - Setup Methods

- (NSMutableArray *)generateItems {
    NSMutableArray *items = [NSMutableArray array];
    for (int i = 0; i < 20; i++) {
        NSString *numberString = [NSString stringWithFormat:@"%@", @(i+1)];
        [items addObject:numberString];
    }
    return items;
}

- (void)setupCollectionView {
    self.collectionView.autoresizingMask = (UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth);
    [self.view addSubview:self.collectionView];
    [self.collectionView registerClass:CollectionViewCell.class forCellWithReuseIdentifier:kCellIdentifier];
}

- (void)setupBin {
    CGFloat size = 60.0f;
    CGFloat padding = 20.0f;

    UIButton *binButton = [UIButton buttonWithType:UIButtonTypeCustom];
    binButton.backgroundColor = [UIColor whiteColor];

    UIImage *trashImage = [UIImage imageNamed:@"trash"];
    [binButton setImage:trashImage forState:UIControlStateNormal];

    CGFloat x = CGRectGetWidth(self.view.bounds) - size - padding;
    CGFloat y = CGRectGetHeight(self.view.bounds) - size - padding;
    binButton.frame = CGRectMake(x, y, size, size);

    binButton.layer.cornerRadius = size * 0.5;
    binButton.layer.masksToBounds = NO;
    binButton.layer.shadowOffset = CGSizeMake(0, 5);
    binButton.layer.shadowRadius = 5;
    binButton.layer.shadowOpacity = .8;

    binButton.autoresizingMask = (UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleLeftMargin);

    self.binButton = binButton;
    [self.view addSubview:binButton];
}

- (void)setupAddButton {
    UIBarButtonItem *addButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAdd target:self action:@selector(addItem)];
    self.navigationItem.rightBarButtonItem = addButton;
}

#pragma mark - CRUD

- (void)removeItem:(id)item {
    NSUInteger indexItem = [self.items indexOfObjectIdenticalTo:item];
    NSIndexPath *indexPath = [NSIndexPath indexPathForItem:indexItem inSection:0];

    [self.items removeObjectIdenticalTo:item];
    [self.collectionView deleteItemsAtIndexPaths:@[indexPath]];
}

- (void)addItem {
    NSString *maxItem = [self.items valueForKeyPath:@"@max.integerValue"];
    NSString *newItem = [NSString stringWithFormat:@"%@", @(maxItem.integerValue + 1)];
    [self.items addObject:newItem];

    NSUInteger indexItem = [self.items indexOfObjectIdenticalTo:newItem];
    NSIndexPath *indexPath = [NSIndexPath indexPathForItem:indexItem inSection:0];
    [self.collectionView insertItemsAtIndexPaths:@[indexPath]];

    [self.collectionView scrollToItemAtIndexPath:indexPath atScrollPosition:UICollectionViewScrollPositionTop animated:YES];
}

#pragma mark - Helper Methods

- (BOOL)isDraggedView:(UIView *)draggedItem hoveringBin:(UIView *)bin {
    CGRect absoluteItemFrame = [self.view convertRect:draggedItem.frame fromView:draggedItem.superview];
    return (CGRectIntersectsRect(bin.frame, absoluteItemFrame));
}

- (void)growBin {
    [self startScaleAnimation:self.binButton withFactor:1.15f];
}

- (void)shrinkBin {
    [self startScaleAnimation:self.binButton withFactor:1.0f];
}

- (void)startScaleAnimation:(UIView *)view withFactor:(CGFloat)factor {
    [UIView
     animateWithDuration:0.2f
     delay:0.0
     options:UIViewAnimationOptionCurveEaseInOut
     animations:^ {
         view.transform = CGAffineTransformScale(CGAffineTransformIdentity, factor, factor);
         [view layoutIfNeeded];
     }
     completion:nil];
}

- (void)alertDeletionForItemAtIndexPath:(NSIndexPath *)indexPath {
    id item = self.items[indexPath.item];

    UIAlertController *alert =
    [UIAlertController
     alertControllerWithTitle:@"Item Deletion"
     message:[NSString stringWithFormat:@"Proceed deleting #%@?", item]
     preferredStyle:UIAlertControllerStyleAlert];

    UIAlertAction *proceedAction =
    [UIAlertAction
     actionWithTitle:@"Yes"
     style:UIAlertActionStyleDefault
     handler:^(UIAlertAction *action) {
         [self removeItem:item];
     }];
    [alert addAction:proceedAction];

    UIAlertAction *cancelAction =
    [UIAlertAction
     actionWithTitle:@"No"
     style:UIAlertActionStyleCancel
     handler:nil];
    [alert addAction:cancelAction];

    [self presentViewController:alert animated:YES completion:nil];
}

#pragma mark - Collection View Methods

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return self.items.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    CollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:kCellIdentifier forIndexPath:indexPath];

    NSString *numberText = self.items[indexPath.item];
    [cell setText:numberText];

    return cell;
}

#pragma mark - Reordering Delegate Methods

- (void)collectionView:(UICollectionView *)collectionView itemAtIndexPath:(NSIndexPath *)fromIndexPath willMoveToIndexPath:(NSIndexPath *)toIndexPath {
    NSLog(@"%s", __PRETTY_FUNCTION__);

    NSString *numberString = self.items[fromIndexPath.item];
    [self.items removeObjectAtIndex:fromIndexPath.item];
    [self.items insertObject:numberString atIndex:toIndexPath.item];
}

- (void)collectionView:(UICollectionView *)collectionView itemAtIndexPath:(NSIndexPath *)fromIndexPath didMoveToIndexPath:(NSIndexPath *)toIndexPath {
    NSLog(@"%s", __PRETTY_FUNCTION__);
}

- (BOOL)collectionView:(UICollectionView *)collectionView itemAtIndexPath:(NSIndexPath *)fromIndexPath canMoveToIndexPath:(NSIndexPath *)toIndexPath {
    NSLog(@"%s", __PRETTY_FUNCTION__);

    return !self.shouldDeleteItem;;
}

- (void)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout willBeginDraggingItemAtIndexPath:(NSIndexPath *)indexPath {
    NSLog(@"%s", __PRETTY_FUNCTION__);
}

- (void)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout didBeginDraggingItemAtIndexPath:(NSIndexPath *)indexPath {
    NSLog(@"%s", __PRETTY_FUNCTION__);
}

- (void)collectionView:(UICollectionView *)collectionView isDraggingView:(UIView *)draggedView ofIndexPath:(NSIndexPath *)indexPath {
    BOOL shouldDeleteItem = [self isDraggedView:draggedView hoveringBin:self.binButton];
    if (shouldDeleteItem == self.shouldDeleteItem) {
        return;
    }

    self.shouldDeleteItem = shouldDeleteItem;
    [self.flowLayout highlightDraggedItem:shouldDeleteItem];
    if (shouldDeleteItem) {
        [self.flowLayout cancelMovingItem];
        [self growBin];
    } else {
        [self shrinkBin];
    }
}

- (void)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout willEndDraggingItemAtIndexPath:(NSIndexPath *)indexPath {
    NSLog(@"%s", __PRETTY_FUNCTION__);

    if (self.shouldDeleteItem) {
        [self alertDeletionForItemAtIndexPath:indexPath];
        self.shouldDeleteItem = NO;
    }
}

- (void)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout didEndDraggingItemAtIndexPath:(NSIndexPath *)indexPath {
    NSLog(@"%s", __PRETTY_FUNCTION__);

    [self.flowLayout highlightDraggedItem:NO];
    [self shrinkBin];
    [self.collectionView reloadData];
}

@end
