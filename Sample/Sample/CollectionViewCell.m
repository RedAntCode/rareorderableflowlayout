#import "CollectionViewCell.h"

@interface CollectionViewCell ()

@property (nonatomic, weak) UILabel *label;

@end

@implementation CollectionViewCell

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        [self setup];
    }

    return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    if (self) {
        [self setup];
    }

    return self;
}

- (void)setup {
    self.backgroundColor = [UIColor whiteColor];

    UILabel *label = [[UILabel alloc] initWithFrame:self.bounds];
    label.font = [UIFont fontWithName:@"Avenir" size:30.0f];
    label.textAlignment = NSTextAlignmentCenter;
    [self.contentView addSubview:label];
    self.label = label;
}

- (void)setText:(NSString *)text {
    self.label.text = text;
}

@end
