#import "CADisplayLink+RA_userInfo.h"
#import <objc/runtime.h>

@implementation CADisplayLink (RA_userInfo)

- (void)setRA_userInfo:(NSDictionary *)RA_userInfo {
    objc_setAssociatedObject(self, "RA_userInfo", RA_userInfo, OBJC_ASSOCIATION_COPY);
}

- (NSDictionary *)RA_userInfo {
    return objc_getAssociatedObject(self, "RA_userInfo");
}

@end
