#import "UIView+Snapshot.h"

@implementation UIView (Snapshot)

- (UIView *)RA_snapshotView {

    // Should be using this, but there is glitch for iOS 8 (iPhone 6, iPhone 6+)
    // http://stackoverflow.com/questions/25873234/snapshotviewafterscreenupdates-glitch-on-ios-8
    //
    // if ([self respondsToSelector:@selector(snapshotViewAfterScreenUpdates:)]) {
    //     return [self snapshotViewAfterScreenUpdates:YES];
    // }

    UIGraphicsBeginImageContextWithOptions(self.bounds.size, self.isOpaque, 0.0f);
    [self.layer renderInContext:UIGraphicsGetCurrentContext()];
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return [[UIImageView alloc] initWithImage:image];
}

@end
