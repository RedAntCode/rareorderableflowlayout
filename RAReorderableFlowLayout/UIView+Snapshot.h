#import <UIKit/UIKit.h>

@interface UIView (Snapshot)

- (UIView *)RA_snapshotView;

@end
